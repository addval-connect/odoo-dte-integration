import requests
import json

# AUTH PARAMS
authUri = "http://copdev.pasteur.cl/WebApiBoletaQA/api/auth.php"
estadoBoletaUri = "http://copdev.pasteur.cl/WebApiBoletaQA/api/setestadoboleta.php"

# BOLETA EJEMPLO
idBoleta = 112233

params = {
    "Usuario": "addval",
    "Clave": "Pwd4ddv4ll"
}

res = requests.post(url = authUri, json = params)
data = res.json()
token = data['Token']

estadoBoleta = {
    "NumeroInterno": idBoleta,
    "NumeroDocumento": 9,
    "EstadoDocumento": "EMITIDA",
    "FechaEstado": "2020-01-20 12:10:00"
}

authHeader = "Bearer " + token

print(authHeader)

res = requests.post(url = estadoBoletaUri, json = estadoBoleta, headers={ "Authorization": authHeader })
data = res.json()

print(data)




