import xmlrpc.client

url = 'https://clinica-pasteur.odoo.com'
db = 'clinica-pasteur'
username = 'jtguzman@addval.cl'
password = 'clinica-pasteur-integracion-iris'

common = xmlrpc.client.ServerProxy('{}/xmlrpc/2/common'.format(url))
common.version()
{
    "server_version": "13.0",
    "server_version_info": [13, 0, 0, "final", 0],
    "server_serie": "13.0",
    "protocol_version": 1,
}

###
# LOG IN
###

uid = common.authenticate(db, username, password, {})

models = xmlrpc.client.ServerProxy('{}/xmlrpc/2/object'.format(url))
res = models.execute_kw(db, uid, password,
    'account.move', 'check_access_rights',
    ['read'], {'raise_exception': False})

print("Authenticated: " + str(res))

###
# LEER UN DTE (BOLETA)
###

#[record] = models.execute_kw(db, uid, password,
#    'account.move', 'read', [2])
#print(record)

#[record] = models.execute_kw(db, uid, password,
#    'account.move.line', 'read', [3])
#print(record)


###
# CREAR UN DTE (BOLETA) A PERSONA NATURAL GENERICA
###

"""
invoiceId = models.execute_kw(db, uid, password, 'account.move', 'create', [{
    'currency_id': 45,
    'date': '2021-01-12 10:10:00', #YYYY-MM-DD HH:MM:SS
    'extract_state': 'no_extract_requested',
    'state': 'draft',
    'journal_id': 1, 
    'move_type': 'out_invoice',
    'l10n_latam_document_type_id': 5,
    'l10n_latam_internal_type': 'invoice',
    'partner_id': 10,
    'invoice_line_ids': [3]
}])
print("Se creó la boleta id: " + str(invoiceId))
"""

#lineId = models.execute_kw(db, uid, password, 'account.move.line', 'create', [{
#    'currency_id': 45,
#    'move_id': 2,
#    'product_id': 2,
#    'quantity': 1,
#   'price_unit': 100
#}])

#print(lineId)

# METHODS EXPERIMENT
record = models.execute_kw(db, uid, password,
    'account.move', 'flush', [12])
print(record)